#ifndef DISTANCECALCULATOR_H
#define DISTANCECALCULATOR_H

#include "../util/macros.h"
#include <utility>

template <typename Kernel> class DistanceCalculator {
public:
  typename Kernel::Segment_3 segmentDist(const typename Kernel::Segment_3 &p,
                                         const typename Kernel::Segment_3 &q);
  typename Kernel::Segment_3 triangleDist(const typename Kernel::Triangle_3 &s,
                                          const typename Kernel::Triangle_3 &t);
  typename Kernel::Segment_3 segmentTriangleDist(const typename Kernel::Segment_3& segment,
                                         const typename Kernel::Triangle_3 &triangle);
  typename Kernel::Segment_3 pointTriangleDist(const typename Kernel::Point_3& point,
                                                 const typename Kernel::Triangle_3 &triangle);
};

#endif
