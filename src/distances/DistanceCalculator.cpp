#include "DistanceCalculator.h"
#include "CGAL/Kernel/global_functions_3.h"
#include <optional>

using CGAL::certainly;
using CGAL::certainly_not;
using CGAL::possibly;
using CGAL::possibly_not;
using CGAL::Uncertain;

template <typename Kernel>
typename Kernel::FT clamp(const typename Kernel::FT& min, const typename Kernel::FT& max, const typename Kernel::FT& value) {
	return CGAL::min(CGAL::max(value, min), max);
}

template <typename Kernel>
std::vector<typename Kernel::Segment_3>
getTriangleSegments(const typename Kernel::Triangle_3& t) {
	USING_KERNEL_TYPEDEFS;
	return std::vector<typename Kernel::Segment_3>{
		Segment_3(t.vertex(0), t.vertex(1)),
			Segment_3(t.vertex(1), t.vertex(2)),
			Segment_3(t.vertex(2), t.vertex(0))
	};
}

// Vladimir J. Lumelsky,
// On fast computation of distance between line segments.
// In Information Processing Letters, no. 21, pages 55-61, 1985.
// ---
// Implementation inspired by Eric Larsen's in PQP: http://gamma.cs.unc.edu/SSV/
template <typename Kernel>
typename Kernel::Segment_3
DistanceCalculator<Kernel>::segmentDist(const typename Kernel::Segment_3& peh, const typename Kernel::Segment_3& quh) {
	USING_KERNEL_TYPEDEFS;

	bool pDegenerate = certainly(peh.squared_length() == 0);
    bool qDegenerate = certainly(quh.squared_length() == 0);

    // Step 1 (a): If one segment degenerates, assume that it corresponds to parameter u
	bool swap = pDegenerate && !qDegenerate;
    const Segment_3 &p = swap ? quh : peh;
    const Segment_3 &q = swap ? peh : quh;

	Point_3 P = p.source();
	Point_3 Q = q.source();
	Vector_3 A = p.to_vector();
	Vector_3 B = q.to_vector();
	Vector_3 T = Q - P;

	FT D_1 = CGAL::scalar_product(A, A);
	FT D_2 = CGAL::scalar_product(B, B);
	FT S_1 = CGAL::scalar_product(A, T);
	FT S_2 = CGAL::scalar_product(B, T);
	FT R   = CGAL::scalar_product(A, B);

    FT t = 0;
    FT u = 0;

    bool skip2 = false;
    bool skip3 = false;
    bool skip4 = false;

    // Step 1: Calculate denominator
	FT denom = D_1 * D_2 - R * R;

    // Step 1 (a): If one segment degenerates, go to step 4
    if (pDegenerate || qDegenerate) {
        skip2 = true;
        skip3 = true;
    }

    // Step 1 (b): If both segments degenerate, go to step 5
	if (pDegenerate && qDegenerate) {
	    skip2 = true;
        skip3 = true;
        skip4 = true;
    }

    // Step 1 (c): If neither segment degenerates and denom is zero, go to step 3
    if (!pDegenerate && !qDegenerate && possibly(denom == 0))
	    skip2 = true;

	// Step 2: Use (11) to compute 11
	if (!skip2) {
        t = certainly(denom == 0)
            ? FTZERO
            : clamp<Kernel>(0, 1, (S_1 * D_2 - S_2 * R) / denom);
    }

	// Step 3: Use (10) to compute u
	if (!skip3) {
        u = certainly(D_2 == 0) ? FTZERO : (t * R - S_2) / D_2;
        // If u is not in [0;1], clamp u.
        FT oldU = u;
        u = clamp<Kernel>(0, 1, u);
        if (certainly(oldU == u))
            skip4 = true;
    }

    // Step 4: Use (10) to compute t. If t is not in [0;1], clamp t.
    if (!skip4)
        t = clamp<Kernel>(0, 1, (u * R + S_1) / D_1);

    // Step 5: Compute actual MinD using (7)
	Point_3 X = P + (A * t);
	Point_3 Y = Q + (B * u);

	return swap ? Segment_3(Y, X) : Segment_3(X, Y);
}

template <typename FT>
std::vector<int> minValues(const std::vector<FT>& values) {
	std::vector<int> result;
	std::vector<FT> absValues;

	for (const FT& value : values)
		absValues.emplace_back(CGAL::abs(value));

	for (int i = 0; i < values.size(); i++) {
		FT absolute = absValues[i];
		bool success = true;
		for (int j = 0; j < values.size(); j++) {
			if (i != j && certainly(absValues[j] < absolute)) {
				success = false;
				break;
			}
		}

		if (success)
			result.emplace_back(i);
	}

	return result;
}

template <typename Kernel, typename Point_n>
std::vector<int> minComponents(const Point_n& point) {
	USING_KERNEL_TYPEDEFS;
	std::vector<int> result;
	std::vector<FT> absValues;

	for (int i = 0; i < point.dimension(); i++)
		absValues.emplace_back(CGAL::abs(point[i]));

	for (int i = 0; i < point.dimension(); i++) {
		FT currentValue = absValues[i];
		bool success = true;
		for (int j = 0; j < point.dimension(); j++) {
			if (i != j && certainly(absValues[j] < currentValue)) {
				success = false;
				break;
			}
		}

		if (success)
			result.emplace_back(i);
	}

	return result;
}

template <typename FT>
Uncertain<bool>
valuesHaveEqualSign(const std::vector<FT>& values) {
	Uncertain<bool> allNegative(true);
	Uncertain<bool> allPositive(true);

	for (const FT& value : values) {
		allNegative = allNegative & (value < 0);
		allPositive = allPositive & (value > 0);
	}

	return allNegative | allPositive;
}

template <typename Kernel, typename Object_3>
std::optional<typename Kernel::Segment_3>
checkInteriorTriangleDistance(const Object_3& object, int vertexCount,
	const typename Kernel::Triangle_3& triangle, bool& shownDisjoint) {
	USING_KERNEL_TYPEDEFS;

	const auto& triangleSegments = getTriangleSegments<Kernel>(triangle);
	Vector_3 tNormal = CGAL::normal(triangle.vertex(0), triangle.vertex(1), triangle.vertex(2));
	FT tNormalLength = tNormal.squared_length();

	if (certainly(tNormalLength <= 0))
		return std::nullopt; // normalLength = area => is triangle degenerate?

	// Project segment vertices on arbitrary triangle vertex
	std::vector<FT> projectedVertices;

	for (int i = 0; i < vertexCount; i++) {
		Vector_3 V = triangle.vertex(0) - object.vertex(i);
		FT projected = CGAL::scalar_product(V, tNormal);
		projectedVertices.emplace_back(projected);
	}

	Uncertain<bool> haveEqualSign = valuesHaveEqualSign<FT>(projectedVertices);
	if (certainly_not(haveEqualSign))
		return std::nullopt;
	else if (certainly(haveEqualSign))
		shownDisjoint = true;

	std::vector<int> points = minValues<FT>(projectedVertices);
	std::optional<Segment_3> result = std::nullopt;

	for (int point : points) {
		bool skipPoint = false;

		// check if projection is within triangle
		for (int i = 0; i < 3; i++) {
			Vector_3 V = object.vertex(point) - triangle.vertex(i);
			Vector_3 Z = CGAL::cross_product(tNormal, triangleSegments[i].to_vector());

			if (certainly(CGAL::scalar_product(V, Z) <= 0)) {
				skipPoint = true;
				break;
			}
		}

		if (skipPoint)
			continue;

		Point_3 P = object.vertex(point);
		Point_3 Q = object.vertex(point) + (tNormal * projectedVertices[point] / tNormalLength);
		Segment_3 tmp = Segment_3(P, Q);

		if (!result.has_value() ||
			possibly(tmp.to_vector().squared_length() < result->to_vector().squared_length()))
			result = tmp;
	}

	return result;
}

template <typename Kernel>
typename Kernel::Segment_3 DistanceCalculator<Kernel>::pointTriangleDist(const typename Kernel::Point_3& point, const typename Kernel::Triangle_3& triangle) {
    USING_KERNEL_TYPEDEFS;
    return segmentTriangleDist(Segment_3(point, point), triangle);
}

template <typename Kernel>
typename Kernel::Segment_3 DistanceCalculator<Kernel>::segmentTriangleDist(const typename Kernel::Segment_3& segment, const typename Kernel::Triangle_3& triangle) {
	USING_KERNEL_TYPEDEFS;

	bool shownDisjoint = false;
	Segment_3 edgeToEdgeDistance;

	// Set first minimum distance safely high
	FT mindd = CGAL::squared_distance(segment.vertex(0), triangle.vertex(0)) + 1;

	// Test all segment distances
	auto triangleSegments = getTriangleSegments<Kernel>(triangle);

	for (int i = 0; i < 3; i++) {
		Segment_3 segDist = segmentDist(segment, triangleSegments[i]);
		FT dd = segDist.to_vector().squared_length();

		if (possibly(dd < mindd)) {
			mindd = dd;
			edgeToEdgeDistance = segDist;

			Point_3 offEdgeVertex = triangle.vertex((i + 2) % 3);
			Vector_3 Z = offEdgeVertex - segDist.point(1);

			FT a = CGAL::scalar_product(Z, segDist.to_vector());
			// TODO: check
			if (certainly(a >= 0))
				return segDist;

			// Check if segment/triangle are disjoint
			// (subtract distance by off-edge distance inside of planes)
			if (certainly(mindd + CGAL::min(a, FTZERO) > 0))
				shownDisjoint = true;
		}
	}

	// Check if one of the closest points is a vertex and the other one is
	// interior to a face (Case 1)
	auto interiorResult = checkInteriorTriangleDistance<Kernel, Segment_3>(segment, 2, triangle, shownDisjoint);

	if (!shownDisjoint) {
		Point_3 any(0, 0, 0);
		return { any, any };
	}
	else if (interiorResult.has_value() && possibly(interiorResult.value().to_vector().squared_length() < mindd)) {
		return interiorResult.value();
	}
	else {
		return edgeToEdgeDistance;
	}
}

// Implementation based on Eric Larsen's in PQP: http://gamma.cs.unc.edu/SSV/
template <typename Kernel>
typename Kernel::Segment_3
DistanceCalculator<Kernel>::triangleDist(const typename Kernel::Triangle_3& s,
	const typename Kernel::Triangle_3& t) {
	USING_KERNEL_TYPEDEFS;

	bool shownDisjoint = false;
    Segment_3 edgeToEdgeDistance;

	// Set first minimum distance safely high
	FT mindd = CGAL::squared_distance(s.vertex(0), t.vertex(0)) + 1;

	auto segmentsOfS = getTriangleSegments<Kernel>(s);
	auto segmentsOfT = getTriangleSegments<Kernel>(t);

	// Check if closest points lie on edges
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			Segment_3 sSegment = segmentsOfS[i];
			Segment_3 tSegment = segmentsOfT[j];
			Segment_3 segDist = segmentDist(sSegment, tSegment);
			FT dd = segDist.to_vector().squared_length();

			if (possibly(dd <= mindd)) {
				mindd = dd;
				edgeToEdgeDistance = segDist;

				// Check if an off-edge vertex lies between planes
				Vector_3 Z;
				Z = s.vertex((i + 2) % 3) - segDist.point(0);
				FT a = CGAL::scalar_product(Z, segDist.to_vector());
				Z = t.vertex((j + 2) % 3) - segDist.point(1);
				FT b = CGAL::scalar_product(Z, segDist.to_vector());

				if (certainly(a <= 0 & b >= 0))
					return segDist;

				// Check if triangles are disjoint
				// (subtract distance by off-edge distances inside of planes)
				if (certainly(mindd - CGAL::max(a, FTZERO) + CGAL::min(b, FTZERO) > 0))
					shownDisjoint = true;
			}
		}
	}

	std::vector<Segment_3> possResults;
	possResults.push_back(edgeToEdgeDistance);

	// Check if one of the closest points is a vertex and the other one is
	// interior to a face (Case 1)
	auto caseOneResult = checkInteriorTriangleDistance<Kernel, Triangle_3>(s, 3, t, shownDisjoint);
	if (caseOneResult.has_value())
		possResults.push_back(caseOneResult.value());

	caseOneResult = checkInteriorTriangleDistance<Kernel, Triangle_3>(t, 3, s, shownDisjoint);
	if (caseOneResult.has_value())
		possResults.push_back(caseOneResult.value());

	if (!shownDisjoint) {
		Point_3 any(0, 0, 0);
		return { any, any };
	}

	Segment_3 result = possResults[0];

	for (int i = 1; i < possResults.size(); i++) {
		const Segment_3& tmp = possResults[i];
		if (possibly(tmp.to_vector().squared_length() < result.to_vector().squared_length()))
			result = tmp;
	}

	return result;
}

template class DistanceCalculator<ExactKernel>;
template class DistanceCalculator<IntervalKernel>;

#include <CGAL/Surface_mesh_default_triangulation_3.h>
typedef CGAL::Surface_mesh_default_triangulation_3 Tr;
typedef Tr::Geom_traits GT;
template class DistanceCalculator<GT>;
