#ifndef SELFINTERSECTION_H
#define SELFINTERSECTION_H

#include "../util/macros.h"

struct SelfIntersection {
  SelfIntersection(FaceIndex faceA, FaceIndex faceB)
      : faceA(faceA), faceB(faceB) {} //, distance(distance), distanceString(distanceString) {}

  FaceIndex faceA, faceB;
//  double distance;
//  std::string distanceString;
};

#endif
