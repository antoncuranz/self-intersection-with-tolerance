#ifndef INTERSECTIONTESTER_H
#define INTERSECTIONTESTER_H

#include "DistanceCalculator.h"
#include "../bvh/AABBTree.h"
#include <iostream>
#include <thread>

#include "SelfIntersection.h"
#include <set>
#include <vector>
#include "../util/util.hpp"

#ifdef USE_MULTITHREADING
#include <tbb/concurrent_vector.h>
#include <tbb/parallel_for.h>
#endif

template <typename Kernel> class AABBTree;
template <typename Kernel> class AABBNode;
template <typename Kernel> class BoundingBox;
class SelfIntersection;

template <typename Kernel> class IntersectionTester {
public:
	IntersectionTester() {};
	inline std::vector<SelfIntersection>
		testForSelfIntersections(const AABBTree<Kernel>& tree);
	inline void setThreshold(double threshold);

private:
	DistanceCalculator<Kernel> mDistanceCalculator;
	double mThreshold = 0;
    typename Kernel::FT mThresholdSq = typename Kernel::FT(0);
	const std::vector<typename Kernel::Point_3>* mVertices;
	const Eigen::MatrixXi* mFaces;
	const AABBTree<Kernel>* mTree;

	template <template <class, class> class AnyVector, class T>
	inline void testNode(const AABBNode<Kernel>& node, FaceIndex faceIndex,
		AnyVector<SelfIntersection, T>& result);
	inline bool insideBoundingBox(const Eigen::Vector3i& face,
		const BoundingBox<Kernel>& boundingBox);
	inline typename Kernel::Triangle_3 toTriangle(const Eigen::Vector3i& face);
	inline typename Kernel::Segment_3 intersectsWithOneCommonVertex(const Eigen::Vector3i& faceA, const Eigen::Vector3i& faceB);
    inline typename Kernel::Segment_3 intersectsWithTwoCommonVertices(const Eigen::Vector3i& faceA, const Eigen::Vector3i& faceB);
};

using CGAL::possibly;

inline int countCommonVertices(const Eigen::Vector3i& faceA, const Eigen::Vector3i& faceB) {
    std::set<int> set{ faceA[0], faceA[1], faceA[2], faceB[0], faceB[1], faceB[2] };
    return 6 - set.size();
}

inline int getCommonVertexIndex(const Eigen::Vector3i& faceA, const Eigen::Vector3i& faceB) {
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			if (faceA[i] == faceB[j])
				return i;

	assert(false);
	return 0;
}

inline int getDistinctVertexIndex(const Eigen::Vector3i& faceA, const Eigen::Vector3i& faceB) {
    for (int i = 0; i < 3; i++) {
        bool isCommon = false;

        for (int j = 0; j < 3; j++)
            if (faceA[i] == faceB[j])
                isCommon = true;

        if (!isCommon)
            return i;
    }

    assert(false);
    return 0;
}

template <typename Kernel>
inline typename Kernel::Segment_3 smallerCombination(
        std::function<typename Kernel::Segment_3(const Eigen::Vector3i&, const Eigen::Vector3i&)> function,
        const Eigen::Vector3i& faceA, const Eigen::Vector3i& faceB) {
    USING_KERNEL_TYPEDEFS;

    Segment_3 distanceA = function(faceA, faceB);
    Segment_3 distanceB = function(faceB, faceA);

    if (possibly(distanceA.to_vector().squared_length() < distanceB.to_vector().squared_length()))
        return distanceA;
    else
        return distanceB;
}

template <typename Kernel>
inline typename Kernel::Segment_3 IntersectionTester<Kernel>::intersectsWithOneCommonVertex(const Eigen::Vector3i& faceA, const Eigen::Vector3i& faceB) {
	USING_KERNEL_TYPEDEFS;

	return smallerCombination<Kernel>([&](const Eigen::Vector3i& faceA, const Eigen::Vector3i& faceB) {
        int commonIndexB = getCommonVertexIndex(faceB, faceA);

        Triangle_3 triangleA((*mVertices)[faceA.x()], (*mVertices)[faceA.y()], (*mVertices)[faceA.z()]);
        Segment_3 oppositeSegmentB((*mVertices)[faceB[(commonIndexB + 1) % 3]], (*mVertices)[faceB[(commonIndexB + 2) % 3]]);

        return mDistanceCalculator.segmentTriangleDist(oppositeSegmentB, triangleA);
    }, faceA, faceB);
}

template <typename Kernel>
inline typename Kernel::Segment_3 IntersectionTester<Kernel>::intersectsWithTwoCommonVertices(const Eigen::Vector3i& faceA, const Eigen::Vector3i& faceB) {
    USING_KERNEL_TYPEDEFS;

    Segment_3 minDist = smallerCombination<Kernel>([&](const Eigen::Vector3i& faceA, const Eigen::Vector3i& faceB) {
        int distinctIndexB = getDistinctVertexIndex(faceB, faceA);

        Triangle_3 triangleA((*mVertices)[faceA.x()], (*mVertices)[faceA.y()], (*mVertices)[faceA.z()]);
        Point_3 oppositeVertexB((*mVertices)[faceB[distinctIndexB]]);

        return mDistanceCalculator.pointTriangleDist(oppositeVertexB, triangleA);
    }, faceA, faceB);

    for (int i = 0; i < 3; i++) {
        int p = faceA[i];
        int q = faceA[(i + 1) % 3];

        for (int j = 0; j < 3; j++) {
            int r = faceB[j];
            int s = faceB[(j + 1) % 3];

            if (p == r || q == s || p == s || q == r)
                continue;

            Segment_3 segA((*mVertices)[p], (*mVertices)[q]);
            Segment_3 segB((*mVertices)[r], (*mVertices)[s]);
            Segment_3 dist = mDistanceCalculator.segmentDist(segA, segB);

            if (possibly(dist.squared_length() < minDist.squared_length())) {
                minDist = dist;
            }
        }
    }

    return minDist;
}

template <typename Kernel>
typename Kernel::Triangle_3
IntersectionTester<Kernel>::toTriangle(const Eigen::Vector3i& face) {
	return typename Kernel::Triangle_3(
		(*mVertices)[face.x()], (*mVertices)[face.y()], (*mVertices)[face.z()]);
}

template <typename Kernel>
inline std::vector<SelfIntersection>
IntersectionTester<Kernel>::testForSelfIntersections(
	const AABBTree<Kernel>& tree) {
	mVertices = &tree.getPointVector();
	mFaces = &tree.getFaces();

    std::cout << "Threshold: " << mThreshold << " (squared: " << mThresholdSq << ")" << std::endl;
    std::cout << "Kernel: " << typeid(Kernel).name();
#ifdef USE_MULTITHREADING
    std::cout << " (with multithreading)" << std::endl;

    tbb::concurrent_vector<SelfIntersection> result;

    tbb::parallel_for(tbb::blocked_range<size_t>(0, mFaces->rows()),
                      [&](const tbb::blocked_range<size_t>& r) {
                          for (size_t i = r.begin(); i != r.end(); ++i)
                              testNode(tree.getRoot(), i, result);
                      });

    return std::vector<SelfIntersection>(result.begin(), result.end());
#else
    std::cout << std::endl;
	std::vector<SelfIntersection> result;

	for (int i = 0; i < mFaces->rows(); i++) {
		testNode(tree.getRoot(), i, result);
	}

	return result;
#endif
}

template <typename Kernel>
template <template <class, class> class AnyVector, class T>
void IntersectionTester<Kernel>::testNode(const AABBNode<Kernel>& node, FaceIndex faceIndex, AnyVector<SelfIntersection, T>& result) {
    USING_KERNEL_TYPEDEFS;
	const Eigen::Vector3i& face = mFaces->row(faceIndex);

	if (node.isLeafNode()) {
		for (const auto& index : node.getFaceIndices()) {
            if (index <= faceIndex)
                continue;

			const Eigen::Vector3i& otherFace = mFaces->row(index);
			int commonVertices = countCommonVertices(face, otherFace);
			Segment_3 dist;

            switch (commonVertices) {
                case 3:
                    continue;
                case 2:
                    dist = intersectsWithTwoCommonVertices(face, otherFace);
                    break;
                case 1:
                    dist = intersectsWithOneCommonVertex(face, otherFace);
                    break;
                default:
                    dist = mDistanceCalculator.triangleDist(toTriangle(face), toTriangle(otherFace));
            }

			typename Kernel::FT sqlen = dist.to_vector().squared_length();

			if (possibly(sqlen <= mThresholdSq)) {
				std::stringstream sstream; sstream << sqlen;
				result.emplace_back(faceIndex, index);//, CGAL::to_double(sqlen), sstream.str());
			}
		}
	}
	else {
		const auto &leftChild = node.getLeftChild();
		if (insideBoundingBox(face, leftChild->getBoundingBox()))
			testNode(*leftChild, faceIndex, result);

		const auto &rightChild = node.getRightChild();
		if (insideBoundingBox(face, rightChild->getBoundingBox()))
			testNode(*rightChild, faceIndex, result);
	}
}

template <typename Kernel>
bool IntersectionTester<Kernel>::insideBoundingBox(
	const Eigen::Vector3i& face, const BoundingBox<Kernel>& boundingBox) {
	BoundingBox<Kernel> faceBox(std::vector<typename Kernel::Point_3>{
		(*mVertices)[face.x()], (*mVertices)[face.y()], (*mVertices)[face.z()] //
	});
	return boundingBox.intersectsBoundingBox(faceBox, mThresholdSq);
}

template <typename Kernel>
void IntersectionTester<Kernel>::setThreshold(double threshold) {
    mThreshold = threshold;
	mThresholdSq = CGAL::square(typename Kernel::FT(threshold));
}

#endif
