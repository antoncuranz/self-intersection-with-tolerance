#ifndef MESHPROCESSOR_H
#define MESHPROCESSOR_H

#include "../bvh/AABBTree.h"
#include "../removal/IntersectionRemover.h"
#include "../distances/IntersectionTester.hpp"
#include "IMeshProcessor.h"
#include "../bvh/AABBHop.h"
#include <igl/opengl/ViewerData.h>

class Visualizer;

template <typename Kernel> class MeshProcessor : public IMeshProcessor {
public:
  MeshProcessor(){};

  void setMesh(const igl::opengl::ViewerData &viewerData) override;
  void findSelfIntersections() override;
  void removeIntersectingFaces(RemovalMode mode) override;
  void aabbCallback(AABBHop hop) override;
  void setThreshold(double threshold) override;
  void setAABBDepth(int depth) override;
  const std::set<FaceIndex> getIntersectingFaces() override;
  const std::vector<SelfIntersection> &getSelfIntersections() override;
  const Eigen::MatrixXd &getVertices() override;
  const Eigen::MatrixXi &getFaces() override;

  const AABBTree<Kernel> *getTree();

  const AABBNode<Kernel> *getCurrentNode() override { return mCurrentNode; };
  const AABBNode<Kernel> *getRootNode() override { return &mTree->getRoot(); };

private:
  std::unique_ptr<AABBTree<Kernel>> mTree;
  IntersectionTester<Kernel> mIntersectionTester;
  IntersectionRemover<Kernel> mIntersectionRemover;
  std::vector<SelfIntersection> mSelfIntersections;
  const AABBNode<Kernel> *mCurrentNode;
  Eigen::MatrixXi mFaces;

  double mThreshold;
  int mAABBDepth;

  FaceIndex mSelectedFace = -1;
  IntersectionIndex mSelectedIntersection = -1;
};

#endif
