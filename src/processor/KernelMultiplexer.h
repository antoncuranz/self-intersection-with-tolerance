#ifndef KERNELMUX_H
#define KERNELMUX_H

#include "IMeshProcessor.h"
#include "MeshProcessor.h"
#include <future>
#include <igl/opengl/ViewerData.h>

class Visualizer;
enum Kernel { Interval, Exact };

class KernelMultiplexer : public IMeshProcessor {
public:
  KernelMultiplexer(){};

  void setMesh(const igl::opengl::ViewerData &viewerData) override;
  void findSelfIntersections() override;
  void removeIntersectingFaces(RemovalMode mode) override;
  void aabbCallback(AABBHop hop) override;
  void setThreshold(double threshold) override;
  void setAABBDepth(int depth) override;
  const std::set<FaceIndex> getIntersectingFaces() override;
  const std::vector<SelfIntersection> &getSelfIntersections() override;
  const Eigen::MatrixXd &getVertices() override;
  const Eigen::MatrixXi &getFaces() override;

  void selectKernel(Kernel kernel);
  Kernel getSelectedKernel();

  const IAABBNode *getCurrentNode() override;
  const IAABBNode *getRootNode() override;

private:
  IMeshProcessor *getCurrentProcessor();

  MeshProcessor<IntervalKernel> mIntervalProcessor;
  MeshProcessor<ExactKernel> mExactProcessor;

  Kernel mSelectedKernel = Interval;

  std::future<void> mFindIntersectionsFuture;
  std::future<void> mSetMeshFuture;
};

#endif
