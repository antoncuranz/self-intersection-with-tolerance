#ifndef IMESHPROCESSOR_H
#define IMESHPROCESSOR_H

#include "../bvh/AABBTree.h"
#include "../distances/IntersectionTester.hpp"
#include "../distances/SelfIntersection.h"
#include "../removal/IntersectionRemover.h"
#include "../bvh/AABBHop.h"
#include <igl/opengl/ViewerData.h>

class IMeshProcessor {
public:
  virtual void setMesh(const igl::opengl::ViewerData &viewerData) = 0;
  virtual void findSelfIntersections() = 0;
  virtual void removeIntersectingFaces(RemovalMode mode) = 0;
  virtual void aabbCallback(AABBHop hop) = 0;
  virtual void setThreshold(double threshold) = 0;
  virtual void setAABBDepth(int depth) = 0;

  virtual const Eigen::MatrixXd &getVertices() = 0;
  virtual const Eigen::MatrixXi &getFaces() = 0;

  virtual const IAABBNode *getCurrentNode() = 0;
  virtual const IAABBNode *getRootNode() = 0;

  virtual const std::set<FaceIndex> getIntersectingFaces() = 0;
  virtual const std::vector<SelfIntersection> &getSelfIntersections() = 0;
};

#endif
