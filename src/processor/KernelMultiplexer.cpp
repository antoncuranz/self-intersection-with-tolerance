#include "KernelMultiplexer.h"
#include <igl/opengl/ViewerData.h>

IMeshProcessor *KernelMultiplexer::getCurrentProcessor() {
  if (mSelectedKernel == Interval)
    return &mIntervalProcessor;
  else
    return &mExactProcessor;
}

void KernelMultiplexer::setMesh(const igl::opengl::ViewerData &viewerData) {
  mIntervalProcessor.setMesh(viewerData);
  mExactProcessor.setMesh(viewerData);
}

void KernelMultiplexer::findSelfIntersections() {
  getCurrentProcessor()->findSelfIntersections();
};

void KernelMultiplexer::removeIntersectingFaces(RemovalMode mode) {
  getCurrentProcessor()->removeIntersectingFaces(mode);
};

void KernelMultiplexer::aabbCallback(AABBHop hop) {
  getCurrentProcessor()->aabbCallback(hop);
};

void KernelMultiplexer::setThreshold(double threshold) {
  mIntervalProcessor.setThreshold(threshold);
  mExactProcessor.setThreshold(threshold);
};

void KernelMultiplexer::setAABBDepth(int depth) {
  mIntervalProcessor.setAABBDepth(depth);
  mExactProcessor.setAABBDepth(depth);
};

void KernelMultiplexer::selectKernel(Kernel kernel) {
  mSelectedKernel = kernel;
  std::cout << "Switched kernel to "
            << (mSelectedKernel == Exact ? "Exact" : "Interval") << std::endl;
};

Kernel KernelMultiplexer::getSelectedKernel() { return mSelectedKernel; }

const Eigen::MatrixXd &KernelMultiplexer::getVertices() {
  return getCurrentProcessor()->getVertices();
}

const Eigen::MatrixXi &KernelMultiplexer::getFaces() {
  return getCurrentProcessor()->getFaces();
}

const IAABBNode *KernelMultiplexer::getCurrentNode() {
  return getCurrentProcessor()->getCurrentNode();
}

const IAABBNode *KernelMultiplexer::getRootNode() {
  return getCurrentProcessor()->getRootNode();
}

const std::set<FaceIndex> KernelMultiplexer::getIntersectingFaces() {
  return getCurrentProcessor()->getIntersectingFaces();
}

const std::vector<SelfIntersection> &KernelMultiplexer::getSelfIntersections() {
  return getCurrentProcessor()->getSelfIntersections();
}
