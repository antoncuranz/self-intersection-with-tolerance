#include "MeshProcessor.h"
#include <igl/opengl/ViewerData.h>
#include <igl/slice.h>
#include <memory>
#include <math.h>

template<typename Kernel>
void MeshProcessor<Kernel>::setMesh(const igl::opengl::ViewerData &viewerData) {
    mTree = std::make_unique<AABBTree<Kernel>>(viewerData.V, viewerData.F,
                                               mAABBDepth);
    mCurrentNode = &mTree->getRoot();
    mFaces = mTree->getFaces();
}

template<typename Kernel>
void MeshProcessor<Kernel>::findSelfIntersections() {
    if (mTree == nullptr)
        return;

    mFaces = mTree->getFaces();

//    TIME_BLOCK("Speedtest (20 times) ",
//        for (int i = 0; i < 20; i++)
//            mIntersectionTester.testForSelfIntersections(*mTree)
//    );

    std::cout << "== DETECTION ==" << std::endl;

    TIME_BLOCK("Detection",
               (mSelfIntersections =
                        mIntersectionTester.testForSelfIntersections(*mTree)));

    mIntersectionRemover.init(mSelfIntersections, *mTree);

    std::cout << "Found " << mSelfIntersections.size() << " self-intersections / "
              << getIntersectingFaces().size() << " self-intersecting faces." << std::endl;
};

template<typename Kernel>
void MeshProcessor<Kernel>::removeIntersectingFaces(RemovalMode mode) {
    if (mTree == nullptr)
        return;

    const auto intersectingFaces = mIntersectionRemover.getSolutionForMode(mode);

    double totalSqArea = 0.0;
    const auto &mVertices = mTree->getPointVector();

    for (const auto faceIndex : intersectingFaces) {
        auto face = mFaces.row(faceIndex);
        typename Kernel::Triangle_3 triangle(
                mVertices[face.x()], mVertices[face.y()], mVertices[face.z()]
        );
        totalSqArea += sqrt(CGAL::to_double(triangle.squared_area()));
    }

    std::cout << "== REMOVAL ==" << std::endl
              << "Removed " << intersectingFaces.size() << " faces." << std::endl
              << "Total area: " << totalSqArea << std::endl;

    Eigen::MatrixXi faces = mTree->getFaces();
    Eigen::MatrixXd vertices = mTree->getVertices();
    Eigen::VectorXi remainingRows(faces.rows() - intersectingFaces.size());

    int index = 0;
    for (int i = 0; i < faces.rows(); i++) {
        if (!intersectingFaces.count(i)) {
            remainingRows[index++] = i;
        }
    }

    igl::slice(faces, remainingRows, 1, mFaces);
};

template<typename Kernel>
void MeshProcessor<Kernel>::aabbCallback(AABBHop hop) {
    if (mTree == nullptr)
        return;

    auto parent = mCurrentNode->getParent();

    if (hop == In) {
        if (!mCurrentNode->isLeafNode())
            mCurrentNode = mCurrentNode->getLeftChild();

    } else if (hop == Out) {
        if (parent != nullptr)
            mCurrentNode = parent;

    } else if (hop == Next) {
        auto sibling = mCurrentNode->getSibling();
        if (sibling != nullptr)
            mCurrentNode = sibling;
    }
};

template<typename Kernel>
void MeshProcessor<Kernel>::setThreshold(double threshold) {
    mIntersectionTester.setThreshold(threshold);
};

template<typename Kernel>
void MeshProcessor<Kernel>::setAABBDepth(int depth) {
    mAABBDepth = depth;
};

template<typename Kernel>
const AABBTree<Kernel> *MeshProcessor<Kernel>::getTree() {
    return mTree.get();
}

template<typename Kernel>
const Eigen::MatrixXd &MeshProcessor<Kernel>::getVertices() {
    return mTree->getVertices();
}

template<typename Kernel>
const Eigen::MatrixXi &MeshProcessor<Kernel>::getFaces() {
    return mFaces;
}

template<typename Kernel>
const std::set<FaceIndex> MeshProcessor<Kernel>::getIntersectingFaces() {
    return mIntersectionRemover.getIntersectingFaces();
}

template<typename Kernel>
const std::vector<SelfIntersection> &
MeshProcessor<Kernel>::getSelfIntersections() {
    return mSelfIntersections;
}

template
class MeshProcessor<IntervalKernel>;

template
class MeshProcessor<ExactKernel>;
