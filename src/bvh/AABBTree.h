#ifndef AABBTREE_H
#define AABBTREE_H

#include "AABBNode.h"

template <typename Kernel> class AABBTree {
public:
  AABBTree(const Eigen::MatrixXd &vertices, const Eigen::MatrixXi &faces,
           int depth);
  const Eigen::MatrixXd &getVertices() const;
  const Eigen::MatrixXi &getFaces() const;
  const std::vector<typename Kernel::Point_3> &getPointVector() const;
  const BoundingBox<Kernel> &getBoundingBox() const;
  const AABBNode<Kernel> &getRoot() const;

private:
  AABBNode<Kernel> mRoot;
  Eigen::MatrixXd mVertices;
  Eigen::MatrixXi mFaces;
  std::vector<typename Kernel::Point_3> mPointVector;
};

#endif
