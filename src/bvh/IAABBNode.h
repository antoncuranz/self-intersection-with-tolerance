#ifndef IAABBNode_H
#define IAABBNode_H

#include "IBoundingBox.h"
#include <vector>

class IAABBNode {
public:
  virtual const std::vector<unsigned int> &getFaceIndices() const = 0;
  virtual const IAABBNode *getParent() const = 0;
  virtual const IAABBNode *getLeftChild() const = 0;
  virtual const IAABBNode *getRightChild() const = 0;
  virtual const IAABBNode *getSibling() const = 0;
  virtual const IBoundingBox &getBoundingBox() const = 0;
};

#endif
