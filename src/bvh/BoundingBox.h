#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

#include "../util/macros.h"
#include "IBoundingBox.h"
#include <utility>
#include <vector>

template <typename Kernel> class BoundingBox : public IBoundingBox {
public:
  explicit BoundingBox(const std::vector<typename Kernel::Point_3> &vertices);
  const typename Kernel::Point_3 &getMaxPoint() const;
  const typename Kernel::Point_3 &getMinPoint() const;
  const typename Kernel::Point_3 &getMidPoint() const;
  const typename Kernel::Point_3 &getEdgeLengths() const;
  typename Kernel::FT getMaxEdgeLength() const;
  bool intersectsVertex(const typename Kernel::Point_3 &vertex,
                        typename Kernel::FT thresholdSq) const;
  bool intersectsBoundingBox(const BoundingBox<Kernel> &boundingBox,
                             typename Kernel::FT thresholdSq) const;
  typename Kernel::FT
  squaredDistance(const typename Kernel::Point_3 &vertex) const;

  const Eigen::Vector3d getMaxPointEigen() const override;
  const Eigen::Vector3d getMinPointEigen() const override;
  const Eigen::Vector3d getMidPointEigen() const override;

private:
  void
  calculateMinMaxPoints(const std::vector<typename Kernel::Point_3> &vertices);
  void calculateMaxEdgeLength();

  typename Kernel::Point_3 mMaxPoint, mMinPoint, mMidPoint, mEdgeLengths;
  typename Kernel::FT mMaxEdgeLength;
};

#endif
