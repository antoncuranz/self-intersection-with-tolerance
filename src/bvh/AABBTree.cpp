#include "AABBTree.h"
#include "../util/util.hpp"
#include <numeric>

template <typename Kernel>
std::vector<typename Kernel::Point_3>
toPointVector(const Eigen::MatrixXd &vertices) {
  std::vector<typename Kernel::Point_3> result;
  for (int i = 0; i < vertices.rows(); i++) {
    auto row = vertices.row(i);
    result.emplace_back(row.x(), row.y(), row.z());
  }
  return result;
}

template <typename Kernel>
AABBTree<Kernel>::AABBTree(const Eigen::MatrixXd &vertices,
                           const Eigen::MatrixXi &faces, int depth)
    : mPointVector(toPointVector<Kernel>(vertices)), mFaces(faces),
      mVertices(vertices), mRoot(toPointVector<Kernel>(vertices), faces,
                                 Util::numberRange(faces.rows()), depth) {}

template <typename Kernel>
const std::vector<typename Kernel::Point_3> &
AABBTree<Kernel>::getPointVector() const {
  return mPointVector;
}

template <typename Kernel>
const Eigen::MatrixXd &AABBTree<Kernel>::getVertices() const {
  return mVertices;
}

template <typename Kernel>
const Eigen::MatrixXi &AABBTree<Kernel>::getFaces() const {
  return mFaces;
}

template <typename Kernel>
const BoundingBox<Kernel> &AABBTree<Kernel>::getBoundingBox() const {
  return mRoot.getBoundingBox();
}

template <typename Kernel>
const AABBNode<Kernel> &AABBTree<Kernel>::getRoot() const {
  return mRoot;
}

template class AABBTree<ExactKernel>;
template class AABBTree<IntervalKernel>;
