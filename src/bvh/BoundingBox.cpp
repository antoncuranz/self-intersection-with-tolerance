#include "BoundingBox.h"

using CGAL::certainly;
using CGAL::possibly;

template <typename Kernel>
BoundingBox<Kernel>::BoundingBox(
    const std::vector<typename Kernel::Point_3> &vertices) {

  calculateMinMaxPoints(vertices);
  mMidPoint = CGAL::midpoint(mMaxPoint, mMinPoint);
  mEdgeLengths = mMaxPoint - (mMinPoint - CGAL::ORIGIN);
  calculateMaxEdgeLength();
}

template <typename Kernel>
void BoundingBox<Kernel>::calculateMinMaxPoints(
    const std::vector<typename Kernel::Point_3> &vertices) {
  typename Kernel::Point_3 somePoint = vertices.front();
  typename Kernel::FT minX = somePoint.x(), minY = somePoint.y(),
                      minZ = somePoint.z();
  typename Kernel::FT maxX = somePoint.x(), maxY = somePoint.y(),
                      maxZ = somePoint.z();

  for (const auto &vertex : vertices) {
    maxX = CGAL::max(maxX, vertex.x());
    maxY = CGAL::max(maxY, vertex.y());
    maxZ = CGAL::max(maxZ, vertex.z());

    minX = CGAL::min(minX, vertex.x());
    minY = CGAL::min(minY, vertex.y());
    minZ = CGAL::min(minZ, vertex.z());
  }

  mMaxPoint = typename Kernel::Point_3(maxX, maxY, maxZ);
  mMinPoint = typename Kernel::Point_3(minX, minY, minZ);
}

template <typename Kernel> void BoundingBox<Kernel>::calculateMaxEdgeLength() {
  mMaxEdgeLength = mEdgeLengths.x();
  mMaxEdgeLength = CGAL::max(mMaxEdgeLength, mEdgeLengths.y());
  mMaxEdgeLength = CGAL::max(mMaxEdgeLength, mEdgeLengths.z());
}

template <typename Kernel>
const typename Kernel::Point_3 &BoundingBox<Kernel>::getMaxPoint() const {
  return mMaxPoint;
}

template <typename Kernel>
const typename Kernel::Point_3 &BoundingBox<Kernel>::getMinPoint() const {
  return mMinPoint;
}

template <typename Kernel>
const typename Kernel::Point_3 &BoundingBox<Kernel>::getMidPoint() const {
  return mMidPoint;
}

template <typename Kernel>
const typename Kernel::Point_3 &BoundingBox<Kernel>::getEdgeLengths() const {
  return mEdgeLengths;
}

template <typename Kernel>
typename Kernel::FT BoundingBox<Kernel>::getMaxEdgeLength() const {
  return mMaxEdgeLength;
}

template <typename Kernel>
bool BoundingBox<Kernel>::intersectsVertex(
    const typename Kernel::Point_3 &vertex,
    typename Kernel::FT thresholdSq) const {
  /* return true; */
  return possibly(squaredDistance(vertex) <= thresholdSq);

  /* for (int i = 0; i < 3; i++)
    if ((certainly(vertex[i] < mMinPoint[i]) &&
         certainly(CGAL::square(mMinPoint[i] - vertex[i]) > thresholdSq)) ||
        (certainly(vertex[i] > mMaxPoint[i]) &&
         certainly(CGAL::square(vertex[i] - mMaxPoint[i]) > thresholdSq)))
      return false; */

  /* return true; */
}

template <typename Kernel>
bool BoundingBox<Kernel>::intersectsBoundingBox(
    const BoundingBox &boundingBox, typename Kernel::FT thresholdSq) const {
  /* Kernel::FT min = CGAL::min(squaredDistance(boundingBox.getMinPoint()), */
  /*                            squaredDistance(boundingBox.getMaxPoint())); */
  /* return possibly(CGAL::square(min) <= thresholdSq); */

  typename Kernel::Point_3 otherMax = boundingBox.getMaxPoint();
  typename Kernel::Point_3 otherMin = boundingBox.getMinPoint();

  for (int i = 0; i < 3; i++) {
    if (certainly(mMaxPoint[i] < otherMin[i]) &&
        certainly(CGAL::square(otherMin[i] - mMaxPoint[i]) > thresholdSq))
      return false;

    if (certainly(otherMax[i] < mMinPoint[i]) &&
        certainly(CGAL::square(mMinPoint[i] - otherMax[i]) > thresholdSq))
      return false;
  }

  return true;
}

template <typename Kernel>
typename Kernel::FT BoundingBox<Kernel>::squaredDistance(
    const typename Kernel::Point_3 &vertex) const {
  typename Kernel::FT result(0), tmp;

  for (int i = 0; i < 3; i++) {
    tmp = vertex[i] - mMaxPoint[i];
    result = CGAL::max(result, tmp);
    tmp = mMinPoint[i] - vertex[i];
    result = CGAL::max(result, tmp);
  }

  return CGAL::square(result);
}

template <typename Kernel>
Eigen::Vector3d toEigenVector(const typename Kernel::Point_3 &p) {
  return Eigen::Vector3d(CGAL::to_double(p.x()), CGAL::to_double(p.y()),
                         CGAL::to_double(p.z()));
}

template <typename Kernel>
const Eigen::Vector3d BoundingBox<Kernel>::getMaxPointEigen() const {
  return toEigenVector<Kernel>(mMaxPoint);
}

template <typename Kernel>
const Eigen::Vector3d BoundingBox<Kernel>::getMinPointEigen() const {
  return toEigenVector<Kernel>(mMinPoint);
}

template <typename Kernel>
const Eigen::Vector3d BoundingBox<Kernel>::getMidPointEigen() const {
  return toEigenVector<Kernel>(mMidPoint);
}

template class BoundingBox<ExactKernel>;
template class BoundingBox<IntervalKernel>;
