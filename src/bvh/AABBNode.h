#ifndef AABBNODE_H
#define AABBNODE_H

#include "../util/macros.h"
#include "BoundingBox.h"
#include "IAABBNode.h"
#include <memory>
#include <vector>

template <typename Kernel> class AABBNode : public IAABBNode {
public:
  AABBNode(AABBNode *parent,
           const std::vector<typename Kernel::Point_3> &vertices,
           const Eigen::MatrixXi &faces,
           const std::vector<unsigned int> &faceIndices, int depth);

  AABBNode(const std::vector<typename Kernel::Point_3> &vertices,
           const Eigen::MatrixXi &faces,
           const std::vector<unsigned int> &faceIndices, int depth)
      : AABBNode(nullptr, vertices, faces, faceIndices, depth){};

  const std::vector<unsigned int> &getFaceIndices() const override;
  const AABBNode<Kernel> *getParent() const override;
  const AABBNode<Kernel> *getLeftChild() const override;
  const AABBNode<Kernel> *getRightChild() const override;
  const AABBNode<Kernel> *getSibling() const override;
  const BoundingBox<Kernel> &getBoundingBox() const override;
  bool isLeafNode() const;

private:
  std::unique_ptr<AABBNode> mLeftChild, mRightChild;
  const std::vector<typename Kernel::Point_3> &mAllVertices;
  const Eigen::MatrixXi &mAllFaces;
  std::vector<unsigned int> mFaceIndices;
  BoundingBox<Kernel> mBoundingBox;
  AABBNode<Kernel> *mParent;
  int mDepth;

  std::vector<typename Kernel::Point_3>
  calculateUsedVertices(const std::vector<unsigned int> &faceIndices);
  void partition();
  typename Kernel::Point_3 getFaceCenter(const Eigen::Vector3i &face);
};

#endif
