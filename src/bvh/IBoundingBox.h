#ifndef IBoundingBox_H
#define IBoundingBox_H

#include <Eigen/src/Core/Matrix.h>

class IBoundingBox {
public:
  virtual const Eigen::Vector3d getMaxPointEigen() const = 0;
  virtual const Eigen::Vector3d getMinPointEigen() const = 0;
  virtual const Eigen::Vector3d getMidPointEigen() const = 0;
};

#endif
