#include "AABBNode.h"
#include <CGAL/Origin.h>
#include <utility>

using CGAL::possibly;

template <typename Kernel>
AABBNode<Kernel>::AABBNode(
    AABBNode<Kernel> *parent,
    const std::vector<typename Kernel::Point_3> &vertices,
    const Eigen::MatrixXi &faces, const std::vector<unsigned int> &faceIndices,
    int depth)
    : mParent(parent), mAllVertices(vertices), mAllFaces(faces),
      mFaceIndices(faceIndices),
      mBoundingBox(calculateUsedVertices(faceIndices)), mDepth(depth) {
  if (faceIndices.size() > 1 && depth > 0)
    partition();
}

template <typename Kernel>
const std::vector<unsigned int> &AABBNode<Kernel>::getFaceIndices() const {
  return mFaceIndices;
}

template <typename Kernel>
const AABBNode<Kernel> *AABBNode<Kernel>::getParent() const {
  return mParent;
}

template <typename Kernel>
const AABBNode<Kernel> *AABBNode<Kernel>::getSibling() const {
  if (mParent == nullptr)
    return nullptr;

  auto leftChild = mParent->getLeftChild();
  auto rightChild = mParent->getRightChild();

  if (leftChild == this)
    return rightChild;
  else
    return leftChild;
}

template <typename Kernel>
const BoundingBox<Kernel> &AABBNode<Kernel>::getBoundingBox() const {
  return mBoundingBox;
}

template <typename Kernel> bool AABBNode<Kernel>::isLeafNode() const {
  return mLeftChild == nullptr || mRightChild == nullptr;
}

template <typename Kernel>
std::vector<typename Kernel::Point_3> AABBNode<Kernel>::calculateUsedVertices(
    const std::vector<unsigned int> &faceIndices) {
  std::vector<typename Kernel::Point_3> result;
  std::vector<unsigned int> usedVertices;

  usedVertices.reserve(3 * faceIndices.size());
  for (const auto &index : faceIndices) {
    Eigen::Vector3i face = mAllFaces.row(index);
    usedVertices.push_back(face.x());
    usedVertices.push_back(face.y());
    usedVertices.push_back(face.z());
  }

  result.reserve(usedVertices.size());
  for (const auto &vertex : usedVertices)
    result.push_back(mAllVertices[vertex]);

  return result;
}

template <typename Kernel>
int maxComponent(const typename Kernel::Point_3 &point) {
  int result = 0;

  if (possibly(point.y() > point[result]))
    result = 1;

  if (possibly(point.z() > point[result]))
    result = 2;

  return result;
}

template <typename Kernel> void AABBNode<Kernel>::partition() {
  std::vector<typename Kernel::Point_3> faceCenters;
  // TODO: talk about this
  for (const auto &index : mFaceIndices) {
    Eigen::Vector3i face = mAllFaces.row(index);
    faceCenters.push_back(getFaceCenter(face));
  }

  BoundingBox<Kernel> centerBox(faceCenters);

  typename Kernel::Point_3 edgeLengths = centerBox.getEdgeLengths();
  int max = maxComponent<Kernel>(edgeLengths);
  auto midPoint = mBoundingBox.getMidPoint();

  std::pair<std::vector<unsigned int>, std::vector<unsigned int>> splitFaces;

  for (int i = 0; i < faceCenters.size(); i++) {
    const typename Kernel::Point_3 &center = faceCenters[i];
    const auto index = mFaceIndices[i];

    if (CGAL::to_double(center[max]) >= CGAL::to_double(midPoint[max]))
      splitFaces.first.emplace_back(index);
    else
      splitFaces.second.emplace_back(index);
  }

  if (!splitFaces.first.empty() && !splitFaces.second.empty()) {
    mLeftChild = std::make_unique<AABBNode>(this, mAllVertices, mAllFaces,
                                            splitFaces.first, mDepth - 1);
    mRightChild = std::make_unique<AABBNode>(this, mAllVertices, mAllFaces,
                                             splitFaces.second, mDepth - 1);
  }
}

template <typename Kernel>
typename Kernel::Point_3
AABBNode<Kernel>::getFaceCenter(const Eigen::Vector3i &face) {
  typedef typename Kernel::Point_3 Point_3;
  typedef typename Kernel::FT FT;
  Point_3 a = mAllVertices[face.x()];
  Point_3 b = mAllVertices[face.y()];
  Point_3 c = mAllVertices[face.z()];
  FT x = (a.x() + b.x() + c.x()) / FT(3);
  FT y = (a.y() + b.y() + c.y()) / FT(3);
  FT z = (a.z() + b.z() + c.z()) / FT(3);
  return Point_3(x, y, z);
}

template <typename Kernel>
const AABBNode<Kernel> *AABBNode<Kernel>::getLeftChild() const {
  return mLeftChild.get();
}

template <typename Kernel>
const AABBNode<Kernel> *AABBNode<Kernel>::getRightChild() const {
  return mRightChild.get();
}

template class AABBNode<ExactKernel>;
template class AABBNode<IntervalKernel>;
