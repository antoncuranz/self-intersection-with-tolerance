#ifndef VISUALIZER_H
#define VISUALIZER_H

#include "../bvh/AABBTree.h"
#include "../removal/IntersectionRemover.h"
#include "../processor/MeshProcessor.h"
#include "macros.h"
#include <igl/opengl/glfw/Viewer.h>
#include <igl/png/readPNG.h>
#include <vector>

template<typename Kernel>
class AABBTree;

template<typename Kernel>
class AABBNode;

template<typename Kernel>
class BoundingBox;

template<typename Kernel>
class IntersectionRemover;

template<typename Kernel>
Eigen::Vector3d toEigenVector(const typename Kernel::Point_3 &p) {
    return Eigen::Vector3d(CGAL::to_double(p.x()), CGAL::to_double(p.y()),
                           CGAL::to_double(p.z()));
}

class Visualizer {
public:
    Visualizer(igl::opengl::glfw::Viewer &viewer) : mViewer(viewer) {
        igl::png::readPNG("matcap.jpg", R, G, B, A);
        igl::png::readPNG("matcap2.jpg", R2, G2, B2, A2);
        mViewer.core().is_animating = true;
        mViewer.core().animation_max_fps = 100;
        mViewer.data().line_width = 5;
        mViewer.core().background_color = Eigen::Vector4f(1., 1., 1. ,1.);
    };

    template<typename Kernel>
    void drawSegments(const std::vector<typename Kernel::Segment_3> &segments) {
        Eigen::MatrixXd sources(segments.size(), 3), targets(segments.size(), 3);

        for (int i = 0; i < segments.size(); i++) {
            typename Kernel::Segment_3 segment = segments[i];

            sources.row(i) = toEigenVector<Kernel>(segment.source());
            targets.row(i) = toEigenVector<Kernel>(segment.target());
        }

        mViewer.data().add_edges(sources, targets, mLineColor);
    }

    void drawBoundingBox(const IBoundingBox &boundingBox) {
        Eigen::Vector3d m = boundingBox.getMinPointEigen();
        Eigen::Vector3d M = boundingBox.getMaxPointEigen();

        // Corners of the bounding box
        Eigen::MatrixXd V_box(8, 3);
        V_box <<
              m(0), m(1), m(2),
                M(0), m(1), m(2),
                M(0), M(1), m(2),
                m(0), M(1), m(2),
                m(0), m(1), M(2),
                M(0), m(1), M(2),
                M(0), M(1), M(2),
                m(0), M(1), M(2);

        // Edges of the bounding box
        Eigen::MatrixXi E_box(12, 2);
        E_box << 0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 1, 5, 2, 6,
                7, 3;

        mViewer.data(mMeshId).clear_edges();
        for (unsigned i = 0; i < E_box.rows(); ++i)
            mViewer.data(mMeshId).add_edges(V_box.row(E_box(i, 0)), V_box.row(E_box(i, 1)),
                                      mLineColor);
    }

    void drawNode(const IAABBNode &node, const IAABBNode &tree) { // tree = root
        drawBoundingBox(node.getBoundingBox());

        auto indices = node.getFaceIndices();
        for (FaceIndex i : indices)
            if (i > mViewer.data(mMeshId).F.rows())
                return;

        highlightFaces(std::set<FaceIndex>(indices.begin(), indices.end()));
    }

    void highlightFaces(const std::set<FaceIndex> &faces) {
        Eigen::MatrixXi hiFaces;
        Eigen::VectorXi remainingRows(faces.size());

        int index = 0;
        for (FaceIndex i : faces)
            remainingRows[index++] = i;

        igl::slice(mViewer.data(mMeshId).F, remainingRows, 1, hiFaces);

        mViewer.data(mHighlightId).clear();
        mViewer.data(mHighlightId).set_mesh(mViewer.data(mMeshId).V, hiFaces);
        mViewer.data(mHighlightId).set_face_based(true);
        mViewer.data(mHighlightId).set_texture(R2, G2, B2, A2);
        mViewer.data(mHighlightId).use_matcap = true;
        mViewer.data(mHighlightId).show_lines = false;
    }

    void setMesh(const Eigen::MatrixXd &vertices, const Eigen::MatrixXi &faces) {
        // Clear all meshes
        mViewer.selected_data_index = mViewer.data_list.size()-1;
        while(mViewer.erase_mesh(mViewer.selected_data_index)){};
        mViewer.data(mHighlightId).clear();

        mMeshId = mViewer.append_mesh();
        mViewer.data(mMeshId).set_mesh(vertices, faces);
        mViewer.data(mMeshId).set_face_based(true);
        mViewer.data(mMeshId).set_texture(R, G, B, A);
        mViewer.data(mMeshId).use_matcap = true;
        mViewer.data(mMeshId).show_lines = false;
    }

    void clearHighlightedFaces() {
        mViewer.data(mHighlightId).clear();
    }

    void clearBoundingBoxEdges() {
        mViewer.data(mMeshId).clear_edges();
    }

    void setMeshVisibility(bool isVisible) {
        mViewer.data(mMeshId).set_visible(isVisible);
    }

    void setHighlightVisibility(bool isVisible) {
        mViewer.data(mHighlightId).set_visible(isVisible);
    }

private:
    int mMeshId, mHighlightId = 0;
    igl::opengl::glfw::Viewer &mViewer;

    Eigen::RowVector3d mLineColor{1.0, 0.506, 0.0};
    // Matcap textures
    Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic> R, G, B, A;
    Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic> R2, G2, B2, A2;
};

#endif
