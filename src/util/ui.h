#ifndef UI_H
#define UI_H

#include "../distances/SelfIntersection.h"
#include "macros.h"

template <typename Kernel> class IntersectionRemover;

namespace UI {

void drawSummary(int nSelfIntersections, int nIntersectingFaces);
void drawIntersectionTable(
    const std::vector<SelfIntersection> &selfIntersections, IntersectionIndex &selectedIntersection);
} // namespace UI

#endif
