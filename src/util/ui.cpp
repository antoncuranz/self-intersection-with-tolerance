#include "ui.h"
#include "../removal/IntersectionRemover.h"
#include "imgui.h"
#include <string>

ImGuiTableFlags tableFlags =
ImGuiTableFlags_ScrollY | ImGuiTableFlags_ScrollX | ImGuiTableFlags_RowBg |
ImGuiTableFlags_BordersOuter | ImGuiTableFlags_BordersV;

ImGuiSelectableFlags selectableFlags =
ImGuiSelectableFlags_SpanAllColumns | ImGuiSelectableFlags_AllowItemOverlap;

void UI::drawSummary(int nSelfIntersections, int nIntersectingFaces) {
	std::string summary = "Found " + std::to_string(nSelfIntersections) + " self-intersections / "
	        + std::to_string(nIntersectingFaces) + " intersecting faces.";
	ImGui::Text("%s", summary.c_str());
}

void UI::drawIntersectionTable(
	const std::vector<SelfIntersection>& selfIntersections, IntersectionIndex& selectedIntersection) {

	ImVec2 outer_size = ImVec2(0.0f, 0.0f);
	char label[32];

	if (ImGui::BeginTable("selfIntersections", 3, tableFlags, outer_size)) {
		ImGui::TableSetupScrollFreeze(0, 1);
		ImGui::TableSetupColumn("intersection", ImGuiTableColumnFlags_None);
		ImGui::TableSetupColumn("face 1", ImGuiTableColumnFlags_None);
		ImGui::TableSetupColumn("face 2", ImGuiTableColumnFlags_None);
		ImGui::TableHeadersRow();

		ImGuiListClipper clipper;
		clipper.Begin(selfIntersections.size());
		while (clipper.Step()) {
			for (int i = clipper.DisplayStart; i < clipper.DisplayEnd; i++) {
				const auto& selfIntersection = selfIntersections[i];
				ImGui::TableNextRow();
				ImGui::TableSetColumnIndex(0);

				sprintf(label, "%d", i);
				if (ImGui::Selectable(label, selectedIntersection == i, selectableFlags))
					selectedIntersection = i;

				ImGui::TableSetColumnIndex(1);
                ImGui::Text("%d", selfIntersection.faceA);
				ImGui::TableSetColumnIndex(2);
                ImGui::Text("%d", selfIntersection.faceB);
			}
		}

		ImGui::EndTable();
	}
}
