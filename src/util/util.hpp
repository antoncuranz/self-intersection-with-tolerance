#ifndef UTIL_H
#define UTIL_H

//#include "../distances/DistanceCalculator.h"
//#include "Visualizer.h"
#include <numeric>
#include <vector>

namespace Util {

inline std::vector<unsigned int> numberRange(unsigned int length) {
    std::vector<unsigned int> result(length);
    std::iota(result.begin(), result.end(), 0);
    return result;
}


/*template <typename Kernel>
void drawTriangleTest(Visualizer &visualizer, igl::opengl::glfw::Viewer &viewer,
                      double x, double y, double z) {
  typedef typename Kernel::Point_3 Point_3;
  typedef typename Kernel::Triangle_3 Triangle_3;
  typedef typename Kernel::Segment_3 Segment_3;

  DistanceCalculator<Kernel> calc;

  Eigen::MatrixXd V(6, 3);
  V << 1, 1, 0, 1, -1, 0, -1, 0, 0, 1, -1, 1, 1, 1, 1, 0.25, 0, 0.25;
  Eigen::MatrixXi F(2, 3);
  F << 0, 1, 2, 3, 4, 5;

  // apply translation
  for (int i = 0; i < 3; i++) {
    V(i, 0) += x;
    V(i, 1) += y;
    V(i, 2) += z;
  }

  Point_3 ra(V(0, 0), V(0, 1), V(0, 2));
  Point_3 rb(V(1, 0), V(1, 1), V(1, 2));
  Point_3 rc(V(2, 0), V(2, 1), V(2, 2));

  Point_3 ta(V(3, 0), V(3, 1), V(3, 2));
  Point_3 tb(V(4, 0), V(4, 1), V(4, 2));
  Point_3 tc(V(5, 0), V(5, 1), V(5, 2));

  Triangle_3 a(ra, rb, rc);
  Triangle_3 b(ta, tb, tc);

  Segment_3 dist = calc.triangleDist(a, b);

  viewer.data().clear_edges();
  viewer.erase_mesh(0);
  viewer.data().clear();
  visualizer.drawSegment<Kernel>(dist);
  viewer.data().set_mesh(V, F);
}*/

} // namespace Util

#endif
