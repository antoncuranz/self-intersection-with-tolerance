#ifndef MACROS_H
#define MACROS_H

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Interval_nt.h>
#include <CGAL/Simple_cartesian.h>
#include <Eigen/Dense>

#define USING_KERNEL_TYPEDEFS \
  typedef typename Kernel::Point_2 Point_2; \
  typedef typename Kernel::Point_3 Point_3; \
  typedef typename Kernel::Vector_3 Vector_3; \
  typedef typename Kernel::Segment_3 Segment_3; \
  typedef typename Kernel::Triangle_3 Triangle_3; \
  typedef typename Kernel::FT FT;

#define FTZERO typename Kernel::FT(0)

typedef CGAL::Exact_predicates_exact_constructions_kernel ExactKernel;
//typedef CGAL::Simple_cartesian<double> DoubleKernel;
typedef CGAL::Simple_cartesian<CGAL::Interval_nt<true>> IntervalKernel;

typedef unsigned int FaceIndex;
typedef unsigned int IntersectionIndex;

#define TIME_BLOCK(msg, code_block)                                            \
  {                                                                            \
    auto t1 = std::chrono::high_resolution_clock::now();                       \
    code_block;                                                                \
    auto t2 = std::chrono::high_resolution_clock::now();                       \
    auto ms_int =                                                              \
        std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);        \
    printf("%s execution time: %lld ms\n", msg, ms_int.count());                 \
  }

#endif
