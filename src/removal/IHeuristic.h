#ifndef I_HEURISTIC_H
#define I_HEURISTIC_H

#include <set>
#include <vector>
#include "../bvh/AABBTree.h"
#include "../distances/SelfIntersection.h"

template<typename Kernel>
class IHeuristic {
public:
    virtual void init(const std::vector<SelfIntersection> &selfIntersections,
                      const AABBTree<Kernel> &tree,
                      const std::set<FaceIndex> &intersectingFaces) {};

    virtual const std::set<FaceIndex> getSolution() = 0;
};

#endif
