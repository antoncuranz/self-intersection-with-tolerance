#ifndef GREEDY_AREA_HEURISTIC_H
#define GREEDY_AREA_HEURISTIC_H

#include "../util/macros.h"
#include "../distances/SelfIntersection.h"
#include "../bvh/AABBTree.h"
#include "AbstractHeuristic.h"
#include <set>
#include <map>
#include <vector>
#include <Eigen/Sparse>

template<typename Kernel>
class WeightedVertexCover : public AbstractHeuristic<Kernel> {
public:
    WeightedVertexCover() = default;
    const std::set<FaceIndex> getSolution() override;

private:
    void postInit() override;
    void constructWeightMap();
    std::map<FaceIndex,double> mWeightMap;
};


#endif
