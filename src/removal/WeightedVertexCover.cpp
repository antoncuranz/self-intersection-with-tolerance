#include "WeightedVertexCover.h"

template<typename Kernel>
void WeightedVertexCover<Kernel>::postInit() {
    constructWeightMap();
}

template<typename Kernel>
void WeightedVertexCover<Kernel>::constructWeightMap() {
    mWeightMap.clear();

    for (FaceIndex faceIndex : *this->mIntersectingFaces) {
        auto face = this->mFaces->row(faceIndex);
        typename Kernel::Triangle_3 triangle(
                (*this->mVertices)[face.x()], (*this->mVertices)[face.y()], (*this->mVertices)[face.z()]
        );
        mWeightMap[faceIndex] = sqrt(CGAL::to_double(triangle.squared_area()));
    }
}

template<typename Kernel>
const std::set<FaceIndex>
WeightedVertexCover<Kernel>::getSolution() {
    std::set<FaceIndex> result;

    while (!mWeightMap.empty()) {
        double minValue = -1;
        unsigned int minIndex = 0;

        // find face with minimal area
        for (const auto& entry : mWeightMap) {
            if (minValue < 0 || entry.second < minValue) {
                minValue = entry.second;
                minIndex = entry.first;
            }
        }

        this->clearVertex(minIndex, [&](long row, long col) {
            mWeightMap.erase(row);
        });

        mWeightMap.erase(minIndex);
        result.insert(minIndex);
    }

    return result;
}

template
class WeightedVertexCover<ExactKernel>;

template
class WeightedVertexCover<IntervalKernel>;