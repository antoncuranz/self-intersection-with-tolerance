#ifndef VERTEXCOVER_H
#define VERTEXCOVER_H

#include "../util/macros.h"
#include "../distances/SelfIntersection.h"
#include "../bvh/AABBTree.h"
#include "AbstractHeuristic.h"
#include <set>
#include <map>
#include <vector>
#include <Eigen/Sparse>

template<typename Kernel>
class VertexCover : public AbstractHeuristic<Kernel> {
public:
    VertexCover() = default;
    const std::set<FaceIndex> getSolution() override;

private:
    void postInit() override;
    void constructValenceMap();
    std::map<FaceIndex,int> mValenceMap;
};


#endif
