#include "IntersectionRemover.h"
#include "../util/util.hpp"

template<typename Kernel>
void IntersectionRemover<Kernel>::init(
        const std::vector<SelfIntersection> &selfIntersections, const AABBTree<Kernel> &tree) {
    mSelfIntersections = &selfIntersections;
    mTree = &tree;
    mIntersectingFaces = std::set<FaceIndex>();
    for (const auto &si : selfIntersections) {
        mIntersectingFaces.insert(si.faceA);
        mIntersectingFaces.insert(si.faceB);
    }
}

template<typename Kernel>
const std::set<FaceIndex>
IntersectionRemover<Kernel>::getSolutionForMode(RemovalMode mode) {
    auto heuristic = mHeuristics[mode];
    heuristic->init(*mSelfIntersections, *mTree, mIntersectingFaces);
    return heuristic->getSolution();
}

template<typename Kernel>
const std::set<FaceIndex>
IntersectionRemover<Kernel>::getIntersectingFaces() const {
    return mIntersectingFaces;
}

template
class IntersectionRemover<ExactKernel>;

template
class IntersectionRemover<IntervalKernel>;
