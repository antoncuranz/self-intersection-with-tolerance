#include "AbstractHeuristic.h"

template<typename Kernel>
void AbstractHeuristic<Kernel>::init(
        const std::vector<SelfIntersection> &selfIntersections,
        const AABBTree<Kernel> &tree,
        const std::set<FaceIndex> &intersectingFaces) {

    mSelfIntersections = selfIntersections;
    mFaces = &tree.getFaces();
    mVertices = &tree.getPointVector();
    mIntersectingFaces = &intersectingFaces;
    constructAdjacencyMatrix();
    postInit();
}

template<typename Kernel>
void AbstractHeuristic<Kernel>::constructAdjacencyMatrix() {
    mAdjacency = Eigen::SparseMatrix<bool>(mFaces->rows(), mFaces->rows());

    std::vector<Eigen::Triplet<bool>> triplets;
    triplets.reserve(mSelfIntersections.size() * 2);

    for (auto intersection : mSelfIntersections) {
        auto faceA = intersection.faceA;
        auto faceB = intersection.faceB;
        triplets.emplace_back(faceA, faceB, true);
        triplets.emplace_back(faceB, faceA, true);
    }

    mAdjacency.setFromTriplets(triplets.begin(), triplets.end());
}

template<typename Kernel>
void AbstractHeuristic<Kernel>::clearVertex(FaceIndex index, const std::function<void(long, long)>& callback) {
    for (Eigen::SparseMatrix<bool>::InnerIterator it(mAdjacency, index); it; ++it) {
        if (!it.value())
            continue;

        it.valueRef() = false;
        mAdjacency.coeffRef(it.col(), it.row()) = false;

        callback(it.row(), it.col());
    }
}

template
class AbstractHeuristic<ExactKernel>;

template
class AbstractHeuristic<IntervalKernel>;
