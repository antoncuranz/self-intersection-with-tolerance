#ifndef INTERSECTING_FACES_HEURISTIC_H
#define INTERSECTING_FACES_HEURISTIC_H

#include "IHeuristic.h"

template<typename Kernel>
class AllIntersectingFaces : public IHeuristic<Kernel> {
    void init(const std::vector<SelfIntersection> &selfIntersections,
              const AABBTree<Kernel> &tree,
              const std::set<FaceIndex> &intersectingFaces) {
        mIntersectingFaces = &intersectingFaces;
    };

    const std::set<FaceIndex> getSolution() {
        return *mIntersectingFaces;
    };

    const std::set<FaceIndex> *mIntersectingFaces;
};


#endif //INTERSECTING_FACES_HEURISTIC_H
