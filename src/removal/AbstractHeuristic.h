#ifndef ABSTRACTHEURISTIC_H
#define ABSTRACTHEURISTIC_H

#include "IHeuristic.h"
#include "../util/macros.h"
#include "../distances/SelfIntersection.h"
#include "../bvh/AABBTree.h"
#include <map>
#include <Eigen/Sparse>

template<typename Kernel>
class AbstractHeuristic : public IHeuristic<Kernel> {
public:
    void init(const std::vector<SelfIntersection> &selfIntersections,
              const AABBTree<Kernel> &tree,
              const std::set<FaceIndex> &intersectingFaces);

    virtual const std::set<FaceIndex> getSolution() = 0;

protected:
    void clearVertex(FaceIndex index, const std::function<void(long, long)>& callback = [](long x, long y) {});
    virtual void postInit() {};

    std::vector<SelfIntersection> mSelfIntersections;
    const Eigen::MatrixXi *mFaces;
    const std::vector<typename Kernel::Point_3> *mVertices;
    const std::set<FaceIndex> *mIntersectingFaces;

private:
    void constructAdjacencyMatrix();

    Eigen::SparseMatrix<bool> mAdjacency;
};

#endif //ABSTRACTHEURISTIC_H
