#ifndef INTERSECTIONREMOVER_H
#define INTERSECTIONREMOVER_H

#include "../util/macros.h"
#include "../distances/SelfIntersection.h"
#include "../bvh/AABBTree.h"
#include "AllIntersectingFaces.h"
#include "VertexCover.h"
#include "WeightedVertexCover.h"
#include <set>
#include <vector>

enum RemovalMode { Remove_allFaces, Remove_VertexCover, Remove_WeightedArea, RemovalMode_SIZE };

template<typename Kernel>
class IntersectionRemover {
public:
    IntersectionRemover() : mHeuristics{
        &mIntersectingFacesHeur,    // Remove_allFaces
        &mVertexCoverHeur,          // Remove_VertexCover
        &mWeightedVertexCoverHeur   // Remove_WeightedArea
    } {};

    void init(const std::vector<SelfIntersection> &selfIntersections, const AABBTree<Kernel> &tree);

    const std::set<FaceIndex> getSolutionForMode(RemovalMode mode);

    const std::set<FaceIndex> getIntersectingFaces() const;

private:
    AllIntersectingFaces<Kernel> mIntersectingFacesHeur;
    VertexCover<Kernel> mVertexCoverHeur;
    WeightedVertexCover<Kernel> mWeightedVertexCoverHeur;

    std::set<FaceIndex> mIntersectingFaces;
    IHeuristic<Kernel> *mHeuristics[RemovalMode_SIZE];

    const std::vector<SelfIntersection> *mSelfIntersections;
    const AABBTree<Kernel> *mTree;
};

#endif
