#include "VertexCover.h"

template<typename Kernel>
void VertexCover<Kernel>::postInit() {
    constructValenceMap();
}

template<typename Kernel>
void VertexCover<Kernel>::constructValenceMap() {
    mValenceMap.clear();

    for (auto intersection : this->mSelfIntersections) {
        mValenceMap[intersection.faceA] += 1;
        mValenceMap[intersection.faceB] += 1;
    }
}

template<typename Kernel>
const std::set<FaceIndex>
VertexCover<Kernel>::getSolution() {
    std::set<FaceIndex> result;

    while (!mValenceMap.empty()) {
        int maxValue = 0;
        unsigned int maxIndex = 0;

        // find face with maximum valence
        for (const auto& entry : mValenceMap) {
            if (entry.second > maxValue) {
                maxValue = entry.second;
                maxIndex = entry.first;
            }
        }

        this->clearVertex(maxIndex, [&](long row, long col) {
            mValenceMap[row] -= 1;
            if (mValenceMap[row] == 0)
                mValenceMap.erase(row);
        });

        mValenceMap.erase(maxIndex);
        result.insert(maxIndex);
    }

    return result;
}

template
class VertexCover<ExactKernel>;

template
class VertexCover<IntervalKernel>;