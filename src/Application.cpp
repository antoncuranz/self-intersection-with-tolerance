#include "Application.h"

#include "imgui.h"
#include "imgui_internal.h"
#include "util/spinner.h"
#include "util/ui.h"
#include <algorithm>

Application::Application() : mVisualizer(mViewer) {
	mMenu.callback_draw_viewer_window = [&]() { drawWindow(); };
	mMenu.callback_draw_custom_window = [&]() { drawResultWindow(); };
	mViewer.plugins.push_back(&mMenu);

	mViewer.callback_init = [&](auto &viewer) { return init(); };
	mKernelMultiplexer.setAABBDepth(mAABBDepth);
}

void drop_callback(GLFWwindow* window, int count, const char** paths) {
    auto self = static_cast<Application*>(glfwGetWindowUserPointer(window));
    std::string path(paths[0]);
    self->loadMesh(path);
    std::cout << "drop_callback: " << paths[0] << std::endl;
}

bool Application::init() {
    glfwSetWindowUserPointer(mViewer.window, static_cast<void*>(this));
    glfwSetDropCallback(mViewer.window, drop_callback);
    return false;
}

void Application::drawCurrentMesh() {
	mVisualizer.setMesh(mKernelMultiplexer.getVertices(), mKernelMultiplexer.getFaces());
	drawAABBNode();
	updateMeshAppearance();
}

void Application::updateMeshAppearance() {
    if (!mShowAABBtree && !mShowResults)
        mVisualizer.clearHighlightedFaces();
	if (!mShowAABBtree)
        mVisualizer.clearBoundingBoxEdges();

	mVisualizer.setMeshVisibility(mShowMesh);
    mVisualizer.setHighlightVisibility(mShowIntersections);
}

void Application::drawAABBNode() {
	mVisualizer.drawNode(*mKernelMultiplexer.getCurrentNode(),
		*mKernelMultiplexer.getRootNode());
}

int Application::launch() {
	mViewer.launch(true, false, "Self-Intersection with tolerance");
	return 0;
}

void disableUiIf(bool condition) {
	if (!condition)
		return;

	ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
	ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
}

void enableUiIf(bool condition) {
	if (!condition)
		return;

	ImGui::PopItemFlag();
	ImGui::PopStyleVar();
}

template<typename R>
bool is_ready(std::future<R> const& f)
{
	return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
}

void Application::loadMesh(const std::string &path) {
    if (mViewer.load_mesh_from_file(path)) {
        mIsMeshLoaded = true;
        mKernelMultiplexer.setMesh(mViewer.data());
        drawCurrentMesh();
        updateMeshAppearance();
    }
}

static void HelpMarker(const char* desc)
{
    if (ImGui::IsItemHovered()  && GImGui->HoveredIdTimer > 1)
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}

void Application::drawWindow() {

	if (mIsBusyFinding && is_ready(mFuture)) {
		mShowResults = true;
		drawCurrentMesh();
		mVisualizer.highlightFaces(mKernelMultiplexer.getIntersectingFaces());
		updateMeshAppearance();
		mIsBusyFinding = false;
	}

	if (mIsBusyRemoving && is_ready(mFuture)) {
		mShowResults = false;
		drawCurrentMesh();
		mIsBusyRemoving = false;
	}

	bool busy = mIsBusyFinding || mIsBusyRemoving;
	bool showResults = mShowResults;

	initImguiStyle();

	ImGui::SetNextWindowPos(ImVec2(180.f * mMenu.menu_scaling(), 10), ImGuiCond_FirstUseEver);
	ImGui::SetNextWindowSize(ImVec2(240, 0), ImGuiCond_FirstUseEver);
	ImGui::Begin("Tools", nullptr, ImGuiWindowFlags_NoSavedSettings);

    disableUiIf(busy);

    if (ImGui::Button("Load##Mesh", ImVec2(ImGui::GetContentRegionAvailWidth() * 0.5f, 0.0f))) {
        std::string fname = igl::file_dialog_open();
        if (fname.length() != 0)
            loadMesh(fname);
    }
    HelpMarker("You can also load meshes using Drag and Drop");

    ImGui::SameLine();
    if (ImGui::Button("Save##Mesh", ImVec2(ImGui::GetContentRegionAvailWidth(), 0.0f)))
        mViewer.open_dialog_save_mesh();

    enableUiIf(busy);

	if (ImGui::CollapsingHeader("Kernel", ImGuiTreeNodeFlags_DefaultOpen)) {
        disableUiIf(busy || !mIsMeshLoaded);

        if (ImGui::RadioButton("Interval", mKernelMultiplexer.getSelectedKernel() ==
			Interval)) {
			mKernelMultiplexer.selectKernel(Interval);
			drawCurrentMesh();
            mShowResults = false;
		}

		ImGui::SameLine();

		if (ImGui::RadioButton("Exact",
			mKernelMultiplexer.getSelectedKernel() == Exact)) {
			mKernelMultiplexer.selectKernel(Exact);
			drawCurrentMesh();
			mShowResults = false;
		}

        enableUiIf(busy || !mIsMeshLoaded);
    }

    if (ImGui::CollapsingHeader("AABB tree")) {
		if (ImGui::InputInt("Depth", &mAABBDepth))
			mKernelMultiplexer.setAABBDepth(mAABBDepth);

        disableUiIf(busy || !mIsMeshLoaded);

		if (ImGui::Checkbox("Visualize tree", &mShowAABBtree)) {
			mKernelMultiplexer.aabbCallback(Stay);
			drawAABBNode();
			updateMeshAppearance();
		}

		if (ImGui::Button("Go in")) {
			mKernelMultiplexer.aabbCallback(In);
			drawAABBNode();
		}

		ImGui::SameLine();

		if (ImGui::Button("Go out")) {
			mKernelMultiplexer.aabbCallback(Out);
			drawAABBNode();
		}

		ImGui::SameLine();
		if (ImGui::Button("Go over")) {
			mKernelMultiplexer.aabbCallback(Next);
			drawAABBNode();
		}

        enableUiIf(busy || !mIsMeshLoaded);
    }

    if (ImGui::CollapsingHeader("Detection",
		ImGuiTreeNodeFlags_DefaultOpen)) {
		if (ImGui::DragScalar("Threshold", ImGuiDataType_Double, &mThreshold,
			0.0005f, &mMinThreshold, &mMaxThreshold, "%.10f",
			ImGuiSliderFlags_Logarithmic)) {
			mKernelMultiplexer.setThreshold(mThreshold);
		}

		disableUiIf(busy || !mIsMeshLoaded);

		if (ImGui::Button("Find")) {
			mIsBusyFinding = true;
            mShowResults = false;
			mFuture = std::async([&]() { mKernelMultiplexer.findSelfIntersections(); });
//            mFuture = std::async([&]() {
//                double threshold = 0.4;
//                while (threshold < 1.55) {
//                    std::cout << "Setting threshold to " << threshold << std::endl;
//                    mKernelMultiplexer.setThreshold(threshold);
//                    mKernelMultiplexer.findSelfIntersections();
//                    threshold += 0.2;
//                }
//            });
		}

		enableUiIf(busy || !mIsMeshLoaded);

		if (busy) {
			ImGui::SameLine();
			ImGui::Spinner("##spinner", 6, 4, ImGui::GetColorU32(ImGuiCol_ButtonHovered));
		}
	}

    if (ImGui::CollapsingHeader("Removal", ImGuiTreeNodeFlags_DefaultOpen)) {
        disableUiIf(busy || !showResults);

        if (ImGui::RadioButton("All intersecting faces", mRemovalMode == Remove_allFaces))
            mRemovalMode = Remove_allFaces;
        if (ImGui::RadioButton("Minimum vertex-cover", mRemovalMode == Remove_VertexCover))
            mRemovalMode = Remove_VertexCover;
        if (ImGui::RadioButton("Min. weighted vc (area)", mRemovalMode == Remove_WeightedArea))
            mRemovalMode = Remove_WeightedArea;

        if (ImGui::Button("Remove faces")) {
            mIsBusyRemoving = true;
            mFuture = std::async([&]() { mKernelMultiplexer.removeIntersectingFaces(mRemovalMode); });
        }

        enableUiIf(busy || !showResults);
    }

	ImGui::End();
}

void Application::drawResultWindow() {
	if (!mShowResults) {
		if (mResultsWereOpen) { // on window closed
			mResultsWereOpen = false;
			mShowMesh = true;
			mShowIntersections = true;
			mSelectedIntersection = 0;
            updateMeshAppearance();
		}
		return;
	}

	mResultsWereOpen = true;

	ImGui::SetNextWindowSize(ImVec2(360, 300), ImGuiCond_FirstUseEver);
	ImGui::Begin("Results", &mShowResults, ImGuiWindowFlags_NoSavedSettings);

    UI::drawSummary(mKernelMultiplexer.getSelfIntersections().size(), mKernelMultiplexer.getIntersectingFaces().size());

    if (ImGui::CollapsingHeader("View options", ImGuiTreeNodeFlags_DefaultOpen)) {
        if (ImGui::Checkbox("Show mesh", &mShowMesh))
            mVisualizer.setMeshVisibility(mShowMesh);

        ImGui::SameLine();

        if (ImGui::Checkbox("Show intersecting faces", &mShowIntersections))
            mVisualizer.setHighlightVisibility(mShowIntersections);
    }

	if (ImGui::CollapsingHeader("Self-Intersections", ImGuiTreeNodeFlags_DefaultOpen))
		UI::drawIntersectionTable(mKernelMultiplexer.getSelfIntersections(), mSelectedIntersection);

	ImGui::End();
}

void Application::initImguiStyle() {
	ImGuiStyle& style = ImGui::GetStyle();
    style.Colors[ImGuiCol_TitleBgActive] = ImVec4(0.0f, 0.0f, 0.0f, 1.00f);
	style.ItemSpacing.y = 8;
	style.FramePadding.x = 12;
	style.FramePadding.y = 5;
	style.CellPadding.x = 12;
	style.CellPadding.y = 5;
	style.FrameRounding = 5;
	style.WindowRounding = 5;
	style.WindowBorderSize = 0;
}
