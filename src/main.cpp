#include "Application.h"

int main(int, char **) {
  Application application;
  return application.launch();
}
