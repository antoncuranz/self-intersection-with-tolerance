#ifndef APPLICATION_H
#define APPLICATION_H

#include "processor/KernelMultiplexer.h"
#include "util/Visualizer.hpp"
#include <future>
#include <igl/opengl/glfw/Viewer.h>
#include <igl/opengl/glfw/imgui/ImGuiMenu.h>

class Application {
public:
  Application();
  int launch();
  void loadMesh(const std::string &path);

private:
  bool init();
  void drawWindow();
  void drawResultWindow();
  void initImguiStyle();
  void drawCurrentMesh();
  void updateMeshAppearance();
  void drawAABBNode();
  static void writeImplicitSurface();

  igl::opengl::glfw::Viewer mViewer;
  igl::opengl::glfw::imgui::ImGuiMenu mMenu;

  Visualizer mVisualizer;
  KernelMultiplexer mKernelMultiplexer;

  // GUI options
  bool mIsMeshLoaded = false;
  bool mShowAABBtree = false;
  double mThreshold = 0, mMinThreshold = 0.0, mMaxThreshold = 1.0;
  bool mShowResults = false, mResultsWereOpen = false;
  int mAABBDepth = 160;
  bool mIsBusyFinding = false;
  bool mIsBusyRemoving = false;
  bool mRemoveDisabled = true;
  bool mShowMesh = true;
  bool mShowIntersections = true;

  IntersectionIndex mSelectedIntersection = 0;

  Kernel mSelectedKernel = Interval;
  RemovalMode mRemovalMode = Remove_allFaces;

  std::future<void> mFuture;
};

#endif
