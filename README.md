# Self-Intersection with Tolerance

## Abstract

Self-intersections are parts of surface meshes that intersect with other parts of itself.
It is desirable to remove such self-intersections, as many applications require meshes to be self-intersection free.
Tools for detecting self-intersections in meshes already exist. However, because of numerical errors in floating-point arithmetic, different tools often provide different answers for self-intersections.

In this thesis, we present an approach to detect and remove self-intersections using a tolerance.
We consider two triangles to be self-intersecting if their distance is smaller than a threshold.
If the threshold is larger than the numerical errors, a sane tool is not expected to find self-intersections after removing all detected ones.
For adjacent triangles, we propose a similar distance measure based on distances between sub-primitives.
To prevent numerical errors when calculating distances, we suggest the use of exact or interval arithmetic.
Additionally, we compare our approach to similar tools and discuss different techniques for removing self-intersections efficiently.

We also provide a C++ implementation of the discussed algorithms.

## Building

1. Install build dependencies (list might be incomplete):
   1. macOS (using Homebrew)
        - `brew install boost tbb mpfr gmp`
   2. Linux (Ubuntu)
        - `apt install libboost-dev libtbb-dev libgmp-dev libmpfr-dev`

2. Clone CGAL (version 5.4 or latest development version) repository into `self-intersection-with-tolerance/`:
```
$ git clone https://github.com/CGAL/cgal.git
```

3. Build using CMake:
```
$ mkdir build
$ cd build
$ cmake .. -DCMAKE_BUILD_TYPE=Release
$ make -j 8
$ ./self-intersection
```
